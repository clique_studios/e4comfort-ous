const gulp         = require('gulp');
const sass         = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

const templateName = 'default';

gulp.task('sass', () => {
  return gulp.src(`wp-content/themes/${templateName}/sass/*.scss`)
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sass())
    .pipe(gulp.dest(`wp-content/themes/${templateName}/dist`));
});

gulp.task('watch', () => gulp.watch(`wp-content/themes/${templateName}/sass/**.*`, gulp.series('sass')));

gulp.task('default', gulp.series('sass'));
