# PHP Composer Wordpress Template

This template simplifies the wordpress installation. This repo is optimized for a containerized environment. Please avoid hard-coding environment variables in wp-config.php, instead, enter configuration values in the `.env` file.

# Password protection

All sites loading via a "bcdev.site" hostname will be forced to use the bluechip password protection located at /etc/apache2/conf/htpasswd.bluechip. This file must exist on each server this template is deployed to.

# File structure

```
/wp-content/themes/default/*
/.env.example
/.gitignore
/.htaccess
/composer.json
/gulpfile.js
/package.json
/wp-config.php
```

Note: The following directories are ephemeral, these will be destroyed & rebuilt on automated deployment of this repository:

```
/public
/vendor
/node_modules
```

# Requirements
* PHP 7.1+
* MySQL 5.7+
* NPM 6.9+
* Gulp

# Installation
1. Copy `.env.example` to `.env`
2. Enter database credentials (`DB_*`) in the newly copied `.env` file & optionally an `APP_URL`
3. Run `composer install` to download wordpress & dependencies
4. If you're using SASS, run `npm install` & `gulp`

# Plugin Installation

Plugins **MUST** be added via the `composer.json` file and installed with the `composer install` command

Available plugins can be found here: https://wpackagist.org/

An example for installing Wordpress SEO is already added in composer.json:

```
...
  "require": {
    "vlucas/phpdotenv": "^2.2",
    "johnpbloch/wordpress": ">=4",
    "wpackagist-plugin/wordpress-seo": ">=7.0.2"
  },
...
```

# Help

* Ensure all configurations in the `.env` file are accurate and you can connect to the MySQL database.
* Use `composer run-script --list` to list all available commands
* Reset environment with `composer reset`. Note: this will delete the public/ vendor/ and node_modules/ folders
