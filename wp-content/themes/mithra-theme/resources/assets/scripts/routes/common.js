// import Swiper from "swiper/dist/js/swiper.js";
import "jquery-match-height/jquery.matchHeight.js"

export default {
    init() {
        // JavaScript to be fired on all pages

        function scrollToAnchor(aid) {
            var aTag = $(aid);
            var focus = true;
            var offset = $("header").height() + parseFloat($("header").css("padding-top")) + parseFloat($("header").css("padding-bottom"));
            $('html,body').animate({scrollTop: aTag.offset().top - offset}, 'slow');
            var first_child = $(aTag.children()[0]);
            var tag = first_child.prop("tagName").toLowerCase();
            if (tag !== "a" && tag !== "button" && tag !== "input" && tag !== "textarea") {
                first_child.attr("tabIndex", -1).focus();
            }
        }

        // Menu/Header
        $("header #hamburger").on("click", function () {
            $("#menu").addClass("open").attr("aria-hidden", false);
            $("#hamburger").attr("aria-expanded", true);
            $("#menu-close").attr("aria-expanded", true);
            setTimeout(function () {
                $("header h1.logo, #hamburger, .main, footer").css("display", "none");
                $("header").css("padding", "0");
                $("#menu").css("position", "static");
            }, 500);
        });

        $("#menu #menu-close").on("click", function () {
            $("#menu").removeClass("open").attr("aria-hidden", true);
            $("#hamburger").attr("aria-expanded", false);
            $("#menu-close").attr("aria-expanded", false);
            $("header, header h1.logo, #hamburger, #menu, .main, footer").removeAttr("style");
        });

        $(".topnav a:not(.wmpl-link)").on("click", function (e) {
            if ($(window).width() <= 1024) {
                var anchor = $(this).attr("href");
                e.preventDefault();
                $("#menu").removeClass("open").attr("aria-hidden", true);
                $("#hamburger").attr("aria-expanded", false);
                $("#menu-close").attr("aria-expanded", false);
                $("header, header h1.logo, #hamburger, #menu, .main, footer").removeAttr("style");
                setTimeout(function () {
                    scrollToAnchor(anchor);
                }, 100);
            }
        });

        // WPML Menu
        // if ($(window).width() > 1024) {
          $('.wpml-ls-current-language').hover(function() {
              $('.wpml-ls-current-language .sub-menu').css("display", "block").addClass('is-visible');
          }, function() {
              $('.wpml-ls-current-language .sub-menu').css("display", "none").removeClass('is-visible');
          });
        // }

        $(window).on("resize", function () {
            if ($(window).width() > 1024) {
                $("#menu").removeClass("open").attr("aria-hidden", false);
                $("#hamburger").attr("aria-expanded", true);
                $("#menu-close").attr("aria-expanded", true);
                $("header, header h1.logo, #hamburger, #menu, .main, footer").removeAttr("style");
            }

            // WPML Menu
            $('.wpml-ls-current-language').hover(function() {
                $('.wpml-ls-current-language .sub-menu').css("display", "block").addClass('is-visible');
            }, function() {
                $('.wpml-ls-current-language .sub-menu').css("display", "none").removeClass('is-visible');
            });
        });

            // WPML Menu changing the text to the languages in the dropdown
            $('.wpml-ls-native[lang="en-gb"]').text('English');
            $('.wpml-ls-native[lang="pl-pl"]').text('Polski');

        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 50) {
                $("header").addClass("scroll");
            } else {
                $("header").removeClass("scroll");
            }
        });

        //Lazy Load Images
        if ($("[data-src]").length > 0) {
            $("[data-src]").each(function () {
                var img = $(this);
                var imgWaypoint = new Waypoint({
                    element: img[0],
                    handler: function () {
                        if (img.attr("src") === "") {
                            var src = img.attr("data-src");
                            img.attr("src", src);
                            if (isIE()) {
                                if (img.parent().hasClass("image-container")) {
                                    var container = img.parent();
                                    container.addClass("ie");
                                    centerImage(container);
                                }
                            }
                        }
                    },
                    offset: "110%",
                });
            })
        }

        //IE Issues
        function isIE() {
            var ua = window.navigator.userAgent; //Check the userAgent property of the window.navigator object
            var msie = ua.indexOf('MSIE '); // IE 10 or older
            var trident = ua.indexOf('Trident/'); //IE 11

            return (msie > 0 || trident > 0);
        }

        if (isIE()) {
            $("html").addClass("ie");
        }

        function centerImage(imageContainer) {
            //set size
            var th = imageContainer.height(),//box height
                tw = imageContainer.width(),//box width
                im,//image
                ih,//initial image height
                iw;//initial image width
            if (imageContainer.children('img, iframe, video, picture').length > 0) {
                im = imageContainer.children('img, iframe, video');
                ih = im.height();
                iw = im.width();
            }

            if (im !== undefined) {
                if ((th / tw) > (ih / iw)) {
                    im.addClass('wh').removeClass('ww');//set height 100%
                } else {
                    im.addClass('ww').removeClass('wh');//set width 100%
                }

                //set offset
                var nh = im.height(),//new image height
                    nw = im.width(),//new image width
                    hd = (nh - th) / 2,//half dif img/box height
                    wd = (nw - tw) / 2;//half dif img/box width
                if (hd < 1) {
                    hd = 0;
                }
                if (wd < 1) {
                    wd = 0;
                }

                im.css({marginLeft: '-' + wd + 'px', marginTop: '-' + hd + 'px'});//offset left
            }
        }

        //Skiplinks
        $("section").each(function () {
            var id = $(this).attr("id");
            if (id !== undefined) {
                var sectionNameArray = id.split("-");
                /*if ($(".single-case-studies").length > 0) {
                    sectionNameArray.pop();
                }*/
                var sectionName = "";
                for (var i = 0; i < sectionNameArray.length; i++) {
                    var str = sectionNameArray[i];
                    str = str.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                        return letter.toUpperCase();
                    });
                    sectionName += str;
                    if (i < sectionNameArray.length - 1) {
                        sectionName += " ";
                    }
                }
                var skiplink = "<li><a href='#" + id + "'>Skip to " + sectionName + "</a></li>";
                $(".skiplinks ul").append(skiplink);
            }
        });

        $(".skiplinks a").on("focus", function () {
            $(".skiplinks").addClass("show");
        });

        $(".skiplinks a").on("blur", function () {
            $(".skiplinks").removeClass("show");
        });

        $(".skiplinks a").on("click", function (e) {
            e.preventDefault();
            scrollToAnchor($(this).attr("href"));
        });


        //UTM
        // function getRefQueryParam(name) {
        //     name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        //     var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        //     var results = regex.exec(location.search);
        //     return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        // };
        //
        // var utmLogoParamQueryString = window.location.href;
        // if (utmLogoParamQueryString.indexOf('?') > 0) {
        //     utmLogoParamQueryString = utmLogoParamQueryString.split('?')[0];
        // }
        //
        // var utmParamQueryString = 'http://screen.rtiresearchawareness.com',
        //     utm_params = '',
        //     utm_source = '',
        //     utm_medium = '',
        //     utm_content = '',
        //     utm_campaign = '',
        //     utm_term = '',
        //     utmScreenerString = $("#pre-qualify-modal .modal__iframe").attr("data-src");
        //
        // (function () {
        //     utm_source = getRefQueryParam("utm_source");
        //     utm_medium = getRefQueryParam("utm_medium");
        //     utm_content = getRefQueryParam("utm_content");
        //     utm_campaign = getRefQueryParam("utm_campaign");
        //     utm_term = getRefQueryParam("utm_term");
        //
        //     if (utm_source) {
        //         utmLogoParamQueryString += '&utm_source=' + utm_source;
        //         utmParamQueryString += '&utm_source=' + utm_source;
        //         utmScreenerString += '&utm_source=' + utm_source;
        //         utm_params += '&utm_source=' + utm_source;
        //     }
        //     if (utm_medium) {
        //         utmLogoParamQueryString += '&utm_medium=' + utm_medium;
        //         utmParamQueryString += '&utm_medium=' + utm_medium;
        //         utmScreenerString += '&utm_medium=' + utm_medium;
        //         utm_params += '&utm_source=' + utm_medium;
        //     }
        //     if (utm_content) {
        //         utmLogoParamQueryString += '&utm_content=' + utm_content;
        //         utmParamQueryString += '&utm_content=' + utm_content;
        //         utmScreenerString += '&utm_content=' + utm_content;
        //         utm_params += '&utm_source=' + utm_content;
        //     }
        //     if (utm_campaign) {
        //         utmLogoParamQueryString += '&utm_campaign=' + utm_campaign;
        //         utmParamQueryString += '&utm_campaign=' + utm_campaign;
        //         utmScreenerString += '&utm_campaign=' + utm_campaign;
        //         utm_params += '&utm_source=' + utm_campaign;
        //     }
        //     if (utm_term) {
        //         utmLogoParamQueryString += '&utm_term=' + utm_term;
        //         utmParamQueryString += '&utm_term=' + utm_term;
        //         utmScreenerString += '&utm_term=' + utm_term;
        //         utm_params += '&utm_source=' + utm_term;
        //     }
        //
        //     if (utmLogoParamQueryString.length > 0) {
        //         utmLogoParamQueryString = utmLogoParamQueryString.replace('&', '?');
        //     }
        //
        //     if (utmParamQueryString.length > 0) {
        //         utmParamQueryString = utmParamQueryString.replace('&', '?');
        //     }
        //
        //     if (utmScreenerString.length > 0) {
        //         utmScreenerString = utmScreenerString.replace('&', '?');
        //     }
        //     if (!utmLogoParamQueryString)
        //         return;
        //
        //     if (!utmParamQueryString)
        //         return;
        //
        //     if (!utmScreenerString)
        //         return;
        //
        //     var logoLink = $('.logo a');
        //
        //     logoLink.each(function () {
        //         if ($(this).attr("href").indexOf('/') === 0 || $(this).attr("href").indexOf(location.host) !== -1) {
        //             $(this).attr("href", utmLogoParamQueryString);
        //         }
        //     });
        //
        //     if ($(".share-links a").length > 0) {
        //         $(".share-links a").each(function () {
        //             if (!$(this).hasClass("email")) {
        //                 $(this).attr("href", $(this).attr("href") + utm_params);
        //             }
        //         });
        //     }
        //
        //     var navLinks = $('a.utm-passthrough');
        //
        //     navLinks.each(function () {
        //         if ($(this).attr("href").indexOf('/') === 0 || $(this).attr("href").indexOf(location.host) !== -1) {
        //             $(this).attr("href", utmLogoParamQueryString);
        //         }
        //     });
        //
        //     $("#pre-qualify-modal .modal__iframe").attr("data-src", utmScreenerString);
        //
        // })();
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        //Nav anchors
        function scrollToAnchor(aid) {
            var aTag = $(aid);
            var focus = true;
            $('html,body').animate({scrollTop: aTag.offset().top - 110}, 'slow');
            var first_child = $(aTag.children()[0]);
            var tag = first_child.prop("tagName").toLowerCase();
            if (tag !== "a" && tag !== "button" && tag !== "input" && tag !== "textarea") {
                first_child.attr("tabIndex", -1).focus();
            }
        }

        $(".topnav a:not(.wmpl-link)").on("click", function (e) {
            e.preventDefault();
            var anchor = $(this).attr("href");
            scrollToAnchor(anchor);
        });

        $(".scrolling-button").on("click", function (e) {
            e.preventDefault();
            var anchor = $(this).attr("href");
            scrollToAnchor(anchor);
        });

        // Tooltip
        $(".tooltip-btn").on("mouseenter", function () {
            $(this).parents('.pq-disclaimer').next().addClass("show");
            $(this).attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().find('.close').attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().attr('aria-hidden', false);
        });

        $(".tooltip-btn").on("focus", function () {
            $(this).parents('.pq-disclaimer').next().addClass("show");
            $(this).attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().find('.close').attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().attr('aria-hidden', false);
        });

        $(".tooltip-btn").on("click", function () {
            if ($(window).width() <= 581) {
                $(".tooltip-inner").removeClass("show").attr("aria-hiddenn", false);
                $(".tooltip-btn, .tooltip-inner .close").attr("aria-expanded", false);
            }
            $(this).parents('.pq-disclaimer').next().addClass("show");
            $(this).attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().find('.close').attr("aria-expanded", true);
            $(this).parents('.pq-disclaimer').next().attr('aria-hidden', false);
        });

        $(".tooltip-inner .close").on("click", function () {
            $(this).parents().removeClass("show");
            $(this).attr("aria-expanded", false);
            $(this).parents('.pq-disclaimer').next().find('.close').attr("aria-expanded", false);
            $(this).parents('.pq-disclaimer').next().attr('aria-hidden', true);
        });

        //FAQ
        $(".question").on("click", function () {
            var item = $(this).parent();
            var question = $(this);
            var answer = question.next(".answer");
            item.toggleClass("show");
            if (item.hasClass("show")) {
                question.attr("aria-expanded", true);
                answer.slideDown().attr("aria-hidden", false);
            } else {
                question.attr("aria-expanded", false);
                answer.slideUp().attr("aria-hidden", true);
            }
        });

        $(".expand").on("click", function () {
            var parent = $(this).parents(".faq-group");
            parent.find(".faq-item").addClass("show");
            parent.find(".question").attr("aria-expanded", true);
            parent.find(".answer").slideDown().attr("aria-hidden", false);
        });

        $(".collapse").on("click", function () {
            var parent = $(this).parents(".faq-group");
            parent.find(".faq-item").removeClass("show");
            parent.find(".question").attr("aria-expanded", false);
            parent.find(".answer").slideUp().attr("aria-hidden", true);
        });

        //Pre Qualify Modal
        var button;
        $(".pre-qualify").on("click", function (e) {
            e.preventDefault();
            button = $(this);
            $("#pre-qualify-modal").addClass("show");
            $("html, body").addClass("no-scroll");
            $(".modal__close a").focus();
            var iframeSrc = $("#pre-qualify-modal").find(".modal__iframe").attr("data-src");
            var iframe = "<iframe src='" + iframeSrc + "' width='100%' height='100%' scrolling='auto'></iframe>";
            $("#pre-qualify-modal .modal__iframe__container").append(iframe);
        });

        $(".modal__close a").on("click", function (e) {
            e.preventDefault();
            $(".modal--exit").addClass("show");
            $(".modal-no").focus();
        });

        $(".modal-leave").on("click", function (e) {
            e.preventDefault();
            $("#pre-qualify-modal, .modal--exit").removeClass("show");
            button.focus();
            $("body").removeClass("no-scroll");
            $("#pre-qualify-modal .modal__iframe__container").empty();
        });

        $(".modal-no").on("click", function (e) {
            $(".modal__close a").focus();
            $(".modal--exit").removeClass("show");
        });

        /**
         * API
         *  Description: Locations are pulled in via an api from Blue Chip. The locations in the API should all be grouped
         *  by region. Can pull in those locations by region with a url param.
         */
        let language = window.location.pathname.replace('/', '');

        let param = '';

        function generateCityStateZipHTML(location={}) {
            let CityStateZipHTML = '';

            if (location.hasOwnProperty('city') && location.city !== null) {
                CityStateZipHTML += `${location.city}, `;
            }

            if (location.hasOwnProperty('state') && location.state !== null) {
                CityStateZipHTML += location.state;
            }

            if (location.hasOwnProperty('zip') && location.zip !== null) {
                CityStateZipHTML += ` ${location.zip}`;
            }

            if (CityStateZipHTML !== '') {
                CityStateZipHTML = `<br>${CityStateZipHTML}`;
            }

            return CityStateZipHTML;
        }

        function generateLocationHTML(locations=[]) {

            if (locations.length > 0) {
                const anchor = document.getElementById('mapLocations');
                let locationItemHtml = '';

                locations.map((location, index) => {
                    let cityStateZip = generateCityStateZipHTML(location);
                    index++;
                    let locationLat = location.lat;
                    let locationLong = location.lon;
                    let buttonLanguage = $('.button-language').attr('data-language');

                    locationItemHtml += `
                    <div id="${locationLat}" class="location-entry col-xs-6 pb-3 utm-passthrough btn_{{ $group_index }}{{ $question_index }}" data-long="${locationLong}" data-value="${index}">
                        <h3 class="match-heading">${location.location_name}</h3>
                        <p class="match-body">
                            ${location.custom_fields.pi_name} <br>
                            ${location.address1}
                            ${cityStateZip}
                        </p>
                        <a class="button reveal-button ${location.custom_fields.unique_identifier}">${buttonLanguage}</a>
                        <div class="contact-reveal" aria-expanded="false">
                            <p class="location-phone"><a target="_blank" href="mailto:${location.phone}">${location.phone}</a></p>
                            <p class="location-number"><a target="_blank" href="tel:${location.location_number}">${location.location_number}</a></p>
                        </div>
                    </div>`;
                });

                console.log(location)

                anchor.innerHTML = locationItemHtml;
            }
        }

        function fetch_locations(param='', page=1) {
            if (param === '') return [];

            fetch(`https://slapi.bluechipdigital.com/api/v1/${param}/locations/list`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'X-SLAPI-Page': page,
                    'X-SLAPI-Per-Page': 100
                },
            })
                .then(res => {
                    return res.json();
                })
                .then(({data, success=false}) => {
                    let locations = [];

                    if (success) {
                        locations = data.locations.filter(location => {
                            return location.active;
                        });

                        generateLocationHTML(locations);
                        let map = initMap(center, locations);
                        // Wrap every 10 divs
                        var divs = $(".swiper-wrapper > .location-entry");
                        for(var i = 0; i < divs.length; i+=6) {
                          divs.slice(i, i+6).wrapAll("<div class='swiper-slide'><div class='row'></div></div>");
                        }


                        // MAP
                        // $(".reveal-button").on("click", function (e) {
                        //     e.preventDefault();
                        //     var item = $(this).parent();
                        //     var question = $(this);
                        //     var answer = question.next(".contact-reveal");
                        //     item.toggleClass("show");
                        //     if (item.hasClass("show")) {
                        //         question.attr("aria-expanded", true);
                        //         answer.slideDown().attr("aria-hidden", false);
                        //     } else {
                        //         question.attr("aria-expanded", false);
                        //         answer.slideUp().attr("aria-hidden", true);
                        //     }
                        // });

                        // Hide buttons if empty
                        $(".contact-reveal .location-phone").filter(function(){
                            return $(this).html().match(/null/) !== null;
                        }).addClass("remove-button");
                        if ($('.contact-reveal .location-phone').hasClass('remove-button')) {
                            $('.reveal-button').addClass('d-none');
                        }

                        // Hide empty location numbers
                        $(".contact-reveal p.location-number").filter(function(){
                            return $(this).html().match(/null/) !== null;
                        }).addClass("d-none");

                        // matchHeight
                        $('.match-heading').matchHeight({
                          byRow: true,
                          property: 'height',
                          target: null,
                          remove: false
                        });

                        $('.match-body').matchHeight({
                          byRow: true,
                          property: 'height',
                          target: null,
                          remove: false
                        });

                        //Zoom to location on the map when user clicks on location
                        $('.location-entry').on('click', function() {
                            let locationLat = $(this).attr('id');
                            let locationLong = $(this).attr('data-long');
                            let location = new google.maps.LatLng(locationLat, locationLong);
                            console.log(location)

                            $('.location-entry').not(this).removeClass('is-active');

                            $(".location-entry.is-active .reveal-button").attr("aria-expanded", true).addClass("show");
                            $(".location-entry.is-active .contact-reveal").slideDown().attr("aria-hidden", false);

                            $(".location-entry:not(.is-active) .reveal-button").attr("aria-expanded", false).removeClass("show");
                            $(".location-entry:not(.is-active) .contact-reveal").slideUp().attr("aria-hidden", true);

                            map.panTo(location);
                            map.setZoom(10);

                            $('.location-entry').removeClass('is-active');
                            $('.contact-reveal').removeClass('show');
                            $(this).addClass('is-active');
                            $(this).find('contact-reveal').addClass('show');
                            $(this).prependTo('#mapLocations');
                            jQuery("#mapLocations").animate({ scrollTop: 0 }, "fast");
                            document.getElementById('map').scrollIntoView();
                        });

                    }

                    return locations;
                })
                .catch((error) => console.log(error));
        }



        $(document).on('click','.reveal-button', function() {
          $(this).toggleClass('open');
          if ( $(this).hasClass('open') ) {
            $(this).next('.contact-reveal').slideDown().attr("aria-hidden", false);
          } else {
            $(this).next('.contact-reveal').slideUp().attr("aria-hidden", true);
          }
        })


        // switch (language) {
        //     case language === 'en':
        //         param = '25';
        //         break;
        //
        //     case language === 'pt-br':
        //         param = '26';
        //         break;
        //
        //     case language === 'sk':
        //         param = '28';
        //         break;
        //
        //     case language === 'es':
        //         param = '29';
        //         break;
        //
        //     case language === 'pl':
        //         param = '27';
        //         break;
        //
        //     default:
        //         param = '25';
        // }

        if ( language === 'en' ) {
        param = '25';
      } else if ( language === 'pt-br/' ) {
        param = '26';
        setTimeout(function() {
          $('.reveal-button').text('Entre em contato com o centro do estudo'); // changing contact button text to Portuguese
        }, 1000)
      } else if ( language === 'sk-sk/' ) {
        param = '28';
        setTimeout(function() {
          $('.reveal-button').text('Obráťte sa na pracovisko'); // changing contact button text to Slovakia
        }, 1000)
      } else if ( language === 'es-es/' ) {
        param = '29';
        setTimeout(function() {
          $('.reveal-button').text('Contactar con el centro'); // changing contact button text to Spain
        }, 1000)
      } else if ( language === 'pl' ) {
        param = '27';
        $('.wpml-ls-33-pl-pl-submenu').addClass('hide');
      } else if ( language === 'pl-pl/' ) { // staging environment uses pl-pl/ - TF
        param = '27';
        $('.wpml-ls-33-pl-pl-submenu').addClass('hide');
        setTimeout(function() {
          $('.reveal-button').text('Skontaktuj się z ośrodkiem'); // changing contact button text to polish
        }, 1000)
      } else if ( language === 'pl-pl' ) { // dev environment uses pl-pl - TF
        param = '27';
        $('.wpml-ls-33-pl-pl-submenu').addClass('hide');
        setTimeout(function() {
          $('.reveal-button').text('Skontaktuj się z ośrodkiem'); // changing contact button text to polish
        }, 1000)
      } else if ( language === 'mithra/pl-pl/' ) { // local environment uses e4comfort-ous/pl/ - TF
        param = '27';
        $('.wpml-ls-33-pl-pl-submenu').addClass('hide');
        setTimeout(function() {
          $('.reveal-button').text('Skontaktuj się z ośrodkiem'); // changing contact button text to polish
        }, 1000)
      } else {
        param = '25';
      }
        console.log(language); // for debugging purposes to see which language and param is being used  - TF
        console.log(param); // for debugging purposes to see which language and param is being used  - TF

        fetch_locations(param, 1);
    },
};
