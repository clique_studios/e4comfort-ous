<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);

// Custom wysiwyg formats
function add_style_select_buttons($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'add_style_select_buttons');


//add custom styles to the WordPress editor
function my_custom_styles($init_array)
{
    $style_formats = array(
        // These are the custom styles
        array(
            'title' => 'Disclaimer',
            'block' => 'p',
            'classes' => 'disclaimer',
            'wrapper' => false,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'my_custom_styles');

//Nav Walker
class Hamburger_Walker extends Walker
{
    public function walk($elements, $max_depth, ...$args)
    {
        $list_array = array();
        foreach ($elements as $item) {
            $classes = implode(" ", $item->classes);
            if (!$item->menu_item_parent):
                $list_array[$item->ID]['ID'] = $item->ID;
                $list_array[$item->ID]['title'] = $item->title;
                $list_array[$item->ID]['url'] = $item->url;
                $list_array[$item->ID]['classes'] = $classes;
                if ($item->attr_title):
                    $list_array[$item->ID]['title_attr'] = $item->attr_title;
                endif;
            else:
                $list_array[$item->menu_item_parent]['children'][$item->ID]['parent_id'] = $item->menu_item_parent;
                $list_array[$item->menu_item_parent]['children'][$item->ID]['title'] = $item->title;
                $list_array[$item->menu_item_parent]['children'][$item->ID]['url'] = $item->url;
                $list_array[$item->menu_item_parent]['children'][$item->ID]['classes'] = $classes;
                if ($item->attr_title):
                    $list_array[$item->menu_item_parent]['children'][$item->ID]['title_attr'] = $item->attr_title;
                endif;
            endif;
        }

        $list = array();
        foreach ($list_array as $item) {
            $list[] .= "<li class='" . $item['classes'] . "'>";
            $list[] .= "<a id='" . $item['ID'] . "' href='" . $item['url'] . "'>";
            if (!empty($item["title_attr"])):
                $list[] .= "<span class='desktop'>" . $item['title'] . "</span>";
                $list[] .= "<span class='responsive'>" . $item['title_attr'] . "</span>";
            else:
                $list[] .= $item['title'];
            endif;
            $list[] .= "</a>";
            if (!empty($item["children"])):
                $list[] .= "<ul id='" . $item['ID'] . "-submenu' class='sub-menu' aria-hidden='true' aria-labelledby='" . $item['ID'] . "'>";
                foreach ($item["children"] as $child) {
                    $list[] = "<li class='" . $child['classes'] . "'>";
                    $list[] .= "<a class='wmpl-link' href='" . $child['url'] . "'>";
                    if ($child["title_attr"]):
                        $list[] .= "<span class='desktop'>";
                    else:
                        $list[] .= "<span>";
                    endif;
                    $list[] .= $child['title'] . "</span>";
                    if ($child["title_attr"]):
                        $list[] .= "<span class='responsive'>" . $child['title_attr'] . "</span>";
                    endif;
                    $list[] .= "</a>";
                    $list[] .= "</li>";
                }
                $list[] .= "</ul>";
                $list[] .= "<button aria-expanded='false' aria-controls='" . $child['parent_id'] . "-submenu'>";
                $list[] .= "<span>Toggle Submenu</span>";
                $list[] .= "</button>";
            endif;
            $list[] .= "</li>";
        }

        return join("\n", $list);
    }
}
