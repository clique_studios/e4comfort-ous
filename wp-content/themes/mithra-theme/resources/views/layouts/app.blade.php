<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
@if(get_field('gtm_body', 'options'))
    {!! get_field('gtm_body', 'options') !!}
@endif
@php do_action('get_header') @endphp
@include('partials.header')
<div class="wrap container" role="document">
    <div class="content">
        <main class="main">
            @yield('content')
        </main>
    </div>
</div>
@if(get_field('pq_form_url', 2))
    <div id="pre-qualify-modal">
        <div class="modal--screener">
            <div class="modal--screener--inner">
                <div class="modal__background"></div>
                <div class="modal__content">
                    <div class="modal__content--inner">
                        <h2 class="type--section-heading">{!! get_field('pq_headline', 2) !!}</h2>
                        <div class="modal__iframe" data-src="{{ get_field('pq_form_url', 2) }}">
                            <div class="modal__iframe__container"></div>
                            <div class="modal__close">
                                <a href="" data-modal="screener-exit">
                                    <svg width="32px" height="32px" viewBox="0 0 32 32">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g fill-rule="nonzero">
                                                <circle id="Oval" fill="#DD5381" cx="16" cy="16" r="16"></circle>
                                                <polygon id="Path" fill="#FFFFFF"
                                                         points="8 12 12 8 24 20 20 24"></polygon>
                                                <polygon id="Path" fill="#FFFFFF"
                                                         points="24 12 20 8 8 20 12 24"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal--exit">
            <div class="modal__background"></div>
            <div class="text-cnt">
                <h2 class="type--section-heading" tabindex="-1"
                    aria-label="{!! get_field('pq_exit_headline', 2) !!}">{!! get_field('pq_exit_headline', 2) !!}</h2>

                <p aria-hidden="true">{!! get_field('pq_exit_copy', 2) !!}</p>

                <div class="modal__buttons">
                    <a href="#" aria-label="No" class="button white btn--large btn--hairline modal-no"
                       data-modal="stay-on-screener">
                        <span tabindex="1">No</span>
                    </a>
                    <a href="#" role="button" aria-label="Yes, close" class="button btn--large modal-leave"
                       data-modal="leave-screener" title="modal-close">
                        <span tabindex="1">Yes, close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif
@php do_action('get_footer') @endphp
@include('partials.footer')
@php wp_footer() @endphp
</body>
</html>
