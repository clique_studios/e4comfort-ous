<section id="banner">
    <div class="bg row">
        <span class="col-xs-6"></span>
        <span class="col-xs-6"></span>
    </div>

    <div class="section-cnt">
        <div class="text-cnt">
            <h2>{!! get_field('banner_headline') !!}</h2>
            {{-- <p class="subheadline">{!! get_field('banner_subheadline') !!}</p> --}}
            <p class="copy">{!! get_field('banner_copy') !!}</p>
            <a href="{!! get_field('pq_cta_url') !!}" class="button scrolling-button utm-passthrough btn_2">{!! get_field('pq_cta_text') !!}</a>
            <div class="tooltip">
                <p class="pq-disclaimer">
                <span>
                    {!! get_field('pq_cta_disclaimer') !!}
                    <button class="tooltip-btn" aria-controls="tooltip-banner" aria-expanded="false">
                        <span id="tooltip-banner-label" class="tooltip-label">Tooltip</span>
                        <img src="@asset('images/tooltip-icon.svg')" alt="tooptip">
                    </button>
                </span>
                </p>
                <div id="tooltip-banner" class="tooltip-inner" aria-hidden="true" aria-labelledby="tooltip-banner-label">
                    <button class="close" aria-controls="tooltip-banner" aria-expanded="false">
                        <span>Close Tooltip</span>
                        <img src="@asset('images/tooltip-close.svg')" alt="close icon">
                    </button>
                    <p>{!! get_field('pq_cta_disclaimer_copy') !!}</p>
                </div>
            </div>
        </div>
        <div class="image-container">
            <img src="" data-src="{{ get_field('banner_image')['url'] }}" alt="{{ get_field('banner_image')['alt'] }}">
        </div>
    </div>
</section>

<section id="about" class="content-section">
    <div class="section-cnt">
        <div class="text-container">
            <h2>{!! get_field('about_headline') !!}</h2>
            {!! get_field('about_copy') !!}
        </div>
        <div class="button-container">
            <a href="{!! get_field('pq_cta_url') !!}" class="button scrolling-button utm-passthrough btn_3">{!! get_field('pq_cta_text') !!}</a>
            <div class="tooltip">
                <p class="pq-disclaimer">
                <span>
                    {!! get_field('pq_cta_disclaimer') !!}
                    <button class="tooltip-btn" aria-controls="tooltip-about" aria-expanded="false">
                        <span id="tooltip-about-label" class="tooltip-label">Tooltip</span>
                        <img src="@asset('images/tooltip-icon-purple.svg')" alt="tooptip">
                    </button>
                </span>
                </p>
                <div id="tooltip-about" class="tooltip-inner"  aria-hidden="true" aria-labelledby="tooltip-about-label">
                    <button class="close" aria-controls="tooltip-about" aria-expanded="false">
                        <span>{{ _e('Close Tooltip', 'sage') }}</span>
                        <img src="@asset('images/tooltip-close.svg')" alt="close icon">
                    </button>
                    <p>{!! get_field('pq_cta_disclaimer_copy') !!}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="about-hot-flushes-and-night-sweats" class="content-section">
    <div class="section-cnt">
        <div class="text-container">
            <h2>{!! get_field("hfns_headline") !!}</h2>
            {!! get_field('hfns_fullcopy') !!}
            <div class="copy-cnt">
                {!! get_field('hfns_copy') !!}
            </div>
            <div class="button-container">
                <a href="{!! get_field('pq_cta_url') !!}" class="button scrolling-button utm-passthrough btn_4">{!! get_field('pq_cta_text') !!}</a>
                <div class="tooltip">
                    <p class="pq-disclaimer">
                <span>
                    {!! get_field('pq_cta_disclaimer') !!}
                    <button class="tooltip-btn" aria-controls="tooltip-hfns" aria-expanded="false">
                        <span id="tooltip-hfns-label" class="tooltip-label">Tooltip</span>
                        <img src="@asset('images/tooltip-icon-purple.svg')" alt="tooptip">
                    </button>
                </span>
                    </p>
                    <div id="tooltip-hfns" class="tooltip-inner"  aria-hidden="true" aria-labelledby="tooltip-hfns-label">
                        <button class="close" aria-controls="tooltip-hfns" aria-expanded="false">
                            <span>Close Tooltip</span>
                            <img src="@asset('images/tooltip-close.svg')" alt="close icon">
                        </button>
                        <p>{!! get_field('pq_cta_disclaimer_copy') !!}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="image-container">
            <img src="" data-src="{{ get_field('hfns_image')['url'] }}" alt="{{ get_field('hfns_image')['alt'] }}">
        </div>
    </div>
</section>
<section id="faq" class="content-section">
      <div class="section-cnt">
          <div class="text-container">
              <h2>{!! get_field('faq_headline') !!}</h2>
              @if(have_rows('faq_groups'))
                  @while(have_rows('faq_groups'))@php the_row() @endphp
                  @php $group_index = get_row_index(); @endphp
                  @php $controls_str = "" @endphp
                  <div class="faq-group">
                      <h3>{!! get_sub_field('faq_headline') !!}</h3>
                      @for($i = 0; $i < count(get_sub_field('faq_items')); $i++)
                          @php $controls_str .= "answer-" . $group_index . "-" . ($i + 1); @endphp
                          @if(($i + 1) < count(get_sub_field('faq_items')))
                              @php $controls_str .= " " @endphp
                          @endif
                      @endfor
                      <div class="buttons-container">
                          <button class="expand" aria-controls="{!! $controls_str !!}">{{ get_sub_field('faq_expand') }}</button>
                          <span></span>
                          <button class="collapse" aria-controls="{!! $controls_str !!}">{{ get_sub_field('faq_collapse') }}</button>
                      </div>
                      @if(have_rows('faq_items'))
                          @while(have_rows('faq_items'))@php the_row() @endphp
                          @php $question_index = get_row_index(); @endphp
                          <div class="faq-item">
                              <button id="question-{{ $group_index }}-{{ $question_index }}" class="question"
                                      aria-controls="answer-{{ $group_index }}-{{ $question_index }}">
                                    <span class="icon"></span>{!! get_sub_field('question') !!}
                              </button>
                              <div id="answer-{{ $group_index }}-{{ $question_index }}" class="answer"
                                   aria-labelledby="question-{{ $group_index }}-{{ $question_index }}"
                                   aria-expanded="false">{!! get_sub_field('answer') !!}
                              </div>
                          </div>
                          @endwhile
                      @endif
                  </div>
                  @endwhile
              @endif
          </div>
      </div>
</section>
<section id="map" class="content-section">
    <div class="section-cnt">
        <div class="text-container">
            <h2>{!! get_field('map_headline') !!}</h2>
            <div class="row">
                <div class="col-lg-8">
                    <div id="google-map"></div>
                    <script src="https://maps.googleapis.com/maps/api/js?key={!! get_field('map_api') !!}" async defer></script>
                    <script type="text/javascript">
                    function initMap(center ={}, locations = []) {
                        //'https://www.google.com/maps/search/?api=1&query='
                        let map = new google.maps.Map(document.getElementById('google-map'), {
                            zoom: 6,
                            center
                        });
                        // let infowindow = new google.maps.InfoWindow({});
                        var infowindow = null;
                        let marker, count;
                        for (let count = 0; count < locations.length; count++) {
                            if (locations[count].lat && locations[count].lon) {
                                let latLng = new google.maps.LatLng(locations[count].lat, locations[count].lon);
                                marker = new google.maps.Marker({
                                    position: latLng,
                                    map: map,
                                    icon: {
                                      url: "@asset('images/map-pin.png')"
                                    },
                                });
                                google.maps.event.addListener(marker, 'click', (function (marker, count) {
                                    return function () {
                                      let pos =  marker.getPosition();
                                      let markerLat = pos.lat();
                                      let locations = document.querySelectorAll(".location-entry");
                                      let location = document.getElementById(markerLat);
                                      let locationChildren = locations.children;
                                      document.querySelectorAll('.location-entry').forEach(location => {
                                        location.classList.remove('is-active');
                                        location.classList.remove('show');
                                        location.children[3].classList.remove('show');
                                      })
                                      jQuery('.reveal-button').removeClass('show');
                                      jQuery(".location-entry:not(.is-active) .contact-reveal").slideUp().attr("aria-hidden", true);
                                      location.classList.add('is-active');
                                      location.children[3].classList.add('show');
                                      let activeLocation = document.querySelector(".show");
                                      let activeLocationParent = activeLocation.parentNode;
                                      let locationContainer = document.querySelector('#mapLocations');
                                      jQuery(activeLocationParent).prependTo(locationContainer);
                                      location.children[3].classList.remove('show');
                                      jQuery("#mapLocations").animate({ scrollTop: 0 }, "fast");
                                      document.getElementById('map').scrollIntoView();
                                      map.setZoom(10);
                                      map.setCenter(marker.getPosition());
                                      // Still a work in progress. - TF
                                      // if ( activeLocationSlide.classList.contains('swiper-slide-active') ) {
                                      //
                                      // } else if ( activeLocationSlide.classList.contains('swiper-slide-next') ) {
                                      //
                                      // } else if ( activeLocationSlide.classList.contains('swiper-slide-prev') ) {
                                      //
                                      // } else if ( activeLocationSlide.classList.contains('swiper-slide') && !activeLocationSlide.classList.contains('swiper-slide-next') && !activeLocationSlide.classList.contains('swiper-slide-prev') && !activeLocationSlide.classList.contains('swiper-slide-active') ) {
                                      //
                                      // }

                                        //infowindow.setContent(locations[count][0]);
                                        infowindow.open(map, marker);
                                    }
                                })(marker, count));

                            }
                        }

                        return map;
                    }
                    </script>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-4 col-lg-offset-0">
                  <div class="location-container swiper-container">
                    <div id="mapLocations" class=" pb-3"></div>
                    {{-- <div class="swiper-pagination pb-3"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div> --}}
                    {{-- <div class="pagination-section">
                      <div class="map-swiper-button-prev swiper-button-prev"><svg xmlns="http://www.w3.org/2000/svg" width="24.1" height="44" viewBox="0 0 24.1 44"><path d="M0,22H0L22,0l2.1,2.1L4.2,22,24.1,41.9,22,44,0,22Z" fill="#dd5381"/></svg></div>
                      <div class="swiper-pagination pb-3"></div>
                      <div class="map-swiper-button-next swiper-button-next"><svg xmlns="http://www.w3.org/2000/svg" width="24.1" height="44" viewBox="0 0 24.1 44"><path d="M24.1,22h0L2.1,44,0,41.9,19.9,22,0,2.1,2.1,0l22,22Z" fill="#dd5381"/></svg></div>
                    </div> --}}
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>

@php
  $buttonLanguage = get_field('map_button_translation', 'options');
  print "<div class='button-language' data-language='$buttonLanguage'></div>";
@endphp

{{-- swiper --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js" integrity="sha256-4sETKhh3aSyi6NRiA+qunPaTawqSMDQca/xLWu27Hg4=" crossorigin="anonymous"></script>
<script>
    // Google Maps
    var center = {lat: {!! get_field('map_latitude') !!}, lng: {!! get_field('map_longitude') !!}};



</script>
