<footer id="footer">
    <div class="footer-inner">
        <div class="logo">
            @if(get_field('logo_white', 'options'))
                <a href="{{ home_url('/') }}" class=" logo_2">
                    <span>E4 Comfort</span>
                    <img src="{{ get_field('logo_white', 'options')['url'] }}"
                         alt="{{ get_field('logo_white', 'options')['alt'] }}">
                </a>
            @endif
        </div>
        @if(get_field('footer_copy', 'options'))
            <p class="copy">{!! get_field('footer_copy', 'options') !!}</p>
        @endif
        <ul class="share-links">
            @if(have_rows('email', 'options'))
                @while(have_rows('email', 'options'))@php the_row() @endphp
                @php
                    $mail_to = "";
                    if(get_sub_field('email_subject') || get_sub_field('email_body')):
                        $mail_to .= "?";
                    endif;
                    if(get_sub_field('email_subject')):
                        $mail_to .= "subject=" . get_sub_field('email_subject');
                    endif;
                    if(get_sub_field('email_body')):
                        if(get_sub_field('email_subject')):
                        $mail_to .= "&";
                        endif;
                        $mail_to .= "body=" . get_sub_field('email_body');
                    endif;
                @endphp
                <li class="ftr-email ftr-email_2">
                    <a href="mailto:{{ $mail_to }}" class="email">
                        <span>{{ get_sub_field('email_address') }}</span>
                        <img src="@asset('images/email-icon.svg')" alt="Email">
                    </a>
                </li>
                @endwhile
            @endif
            <li class="ftr-facebook ftr-facebook_2">
                <a href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}&quote={{ get_field
                    ('facebook_message',  'options') }}" target="_blank">
                    <span>Facebook</span>
                    <img src="@asset('images/facebook-logo.png')" alt="Facebook">
                </a>
            </li>
            <li class="ftr-twitter ftr-twitter_2">
                <a href="http://twitter.com/share?text={{ get_field('twitter_message', 'options') }}&url=''" target="_blank">
                    <span>Twitter</span>
                    <img src="@asset('images/twitter-icon.svg')" alt="Twitter">
                </a>
            </li>
        </ul>
        <p class="copyright">{!! get_field('copyright', 'options') !!}</p>
        @if (has_nav_menu('footer_links'))
            {!! wp_nav_menu(['theme_location' => 'footer_links', 'menu_class' => 'footer-links']) !!}
        @endif
    </div>
</footer>