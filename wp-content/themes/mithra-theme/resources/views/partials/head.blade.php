<head>
  @if(get_field('gtm_header', 'options'))
      {!! get_field('gtm_header', 'options') !!}
  @endif

      <?php if (ICL_LANGUAGE_CODE == "en-gb") :?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-THWSQV9');</script>
        <!-- End Google Tag Manager -->
      <?php endif; ?>

      <?php if (ICL_LANGUAGE_CODE == "pl-pl") :?>
        <!-- Google Tag Manager-->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WZQQV8V');</script>
        <!-- End Google Tag Manager -->
      <?php endif; ?>


    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href=" https://use.typekit.net/xrj8rat.css">
    {{-- Adobe font typekit --}}
    @php wp_head() @endphp
</head>
