<div class="skiplinks">
    <h2>Skip Links</h2>
    <ul></ul>
</div>
<header class="header">
    <div class="header-inner">
        <h1 class="logo">
            @if(get_field('logo_green', 'options'))
                <a href="{{ home_url('/') }}" class="logo_1">
                    <span>E4 Comfort</span>
                    <img src="{{ get_field('logo_green', 'options')['url'] }}"
                         alt="{{ get_field('logo_green', 'options')['alt'] }}">
                </a>
            @endif
        </h1>
        <button id="hamburger" aria-controls="menu" aria-expanded="true" aria-labelledby="hamburger">
            <span>Open Menu</span>
            <img src="@asset('images/tablet-nav.svg')" alt="Open Menu">
        </button>
        <div id="menu" aria-hidden="false">
            <div class="top responsive-menu-element">
                <div class="logo">
                    @if(get_field('logo_white', 'options'))
                        <a href="{{ home_url('/') }}">
                            <span>E4 Comfort</span>
                            <img src="{{ get_field('logo_white', 'options')['url'] }}"
                                 alt="{{ get_field('logo_white', 'options')['alt'] }}">
                        </a>
                    @endif
                </div>
                <button id="menu-close" class="close" aria-controls="menu" aria-expanded="true"
                        aria-labelledby="menu-close">
                    <span>Close Menu</span>
                    <img src="@asset('images/close.svg')" alt="close menu">
                </button>
            </div>
            <div class="responsive-green">
                <nav class="topnav">
                    @if (has_nav_menu('primary_navigation'))
                        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => 'false',
                 'walker' => new Hamburger_Walker ]) !!}
                    @endif
                </nav>
                <a href="{!! get_field('pq_cta_url') !!}" class="button scrolling-button utm-passthrough btn_1">{!! get_field('pq_cta_text') !!}</a>
                <div class="responsive-menu-element bottom">
                    @if(get_field('menu_copy', 'options'))
                        <p class="copy">{!! get_field('menu_copy', 'options') !!}</p>
                    @endif
                    <ul class="share-links">
                        @if(have_rows('email', 'options'))
                            @while(have_rows('email', 'options'))@php the_row() @endphp
                            @php
                                $mail_to = get_sub_field('email_address');
                                if(get_sub_field('email_subject') || get_sub_field('email_body')):
                                    $mail_to .= "?";
                                endif;
                                if(get_sub_field('email_subject')):
                                    $mail_to .= "subject=" . get_sub_field('email_subject');
                                endif;
                                if(get_sub_field('email_body')):
                                    if(get_sub_field('email_subject')):
                                    $mail_to .= "&";
                                    endif;
                                    $mail_to .= "body=" . get_sub_field('email_subject');
                                endif;
                            @endphp
                            <li class="ftr-email ftr-email_1">
                                <a href="mailto:{{ $mail_to }}" class="email">
                                    <span>{{ get_sub_field('email_address') }}</span>
                                    <img src="@asset('images/email-icon.svg')" alt="Email">
                                </a>
                            </li>
                            @endwhile
                        @endif
                        <li class="ftr-facebook ftr-facebook_1">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ get_the_permalink() }}&quote={{ get_field
                    ('facebook_message',  'options') }}">
                                <span>Facebook</span>
                                <img src="@asset('images/facebook-icon.svg')" alt="Facebook">
                            </a>
                        </li>
                        <li class="ftr-twitter ftr-twitter_1">
                            <a href="http://twitter.com/share?text={{ get_field('twitter_message', 'options')
                            }}&url=''" target="_blank">
                                <span>Twitter</span>
                                <img src="@asset('images/twitter-icon.svg')" alt="Twitter">
                            </a>
                        </li>
                    </ul>
                    <p class="copyright">{!! get_field('copyright', 'options') !!}</p>
                    @if (has_nav_menu('footer_links'))
                        {!! wp_nav_menu(['theme_location' => 'footer_links', 'menu_class' => 'footer-links']) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
